const jsonServer = require('json-server');
const server = jsonServer.create();

server.use(jsonServer.defaults());
server.use(jsonServer.router('data.json'));
server.listen(4000, () => {
  console.log('Server is running on http://localhost:4000');
});
